/**
 * Created by lixiaodong on 16/4/7.
 */
var GameSceneUI = cc.Layer.extend({

    menu        :   null,
    bg          :   null,
    bg_logo     :   null,
    bg_pot      :   null,
    buyButton   :   null,
    menuButton  :   null,
    talkButton  :   null,
    helpButton  :   null,
    emotionButton   :   null,
    menu_bg     :   null,
    baseDir     :   null,
    exitMenu    :   null,
    buyChips    :   null,
    menu_layer  :   null,
    card_back_small :   null,
    card1       :   null, //当前玩家第一张牌
    card2       :   null,//当前玩家第二张牌


    ctor    : function () {
        this._super();

        this.baseDir     =   "#img/room/";

        this.initBgRoom();
        this.initBgLogo();
        this.initJackPot();

        this.initBuyChipsMenu();
        this.initRoomMenu();
        this.initHelpMenu();
        this.initTalkMenu();
        this.initEmotionMenu();

        this.initPlayersPosition();

        return true;
    },
    initBgRoom: function () {
        if(!this.bg){
            var size = cc.director.getWinSize();
            this.bg = new cc.Sprite(this.baseDir+'bg-room.png');
            this.bg.x = size.width / 2;
            this.bg.y = size.height / 2;
            this.addChild(this.bg);
        }
    },
    initBgLogo: function () {
        //logo
        if(!this.bg_logo){
            var size = cc.director.getWinSize();
            this.bg_logo = new cc.Sprite(this.baseDir+'bg-logo.png');
            this.bg_logo.x = size.width / 2;
            this.bg_logo.y = this.bg.y;

            this.addChild(this.bg_logo);
        }
    },
    initJackPot: function () {
        //奖池图标
        if(!this.bg_pot){
            var size = cc.director.getWinSize();
            this.bg_pot = new cc.Sprite(this.baseDir+'bg-pot.png');
            this.bg_pot.x = size.width / 2;
            this.bg_pot.y = this.bg_logo.y + 300;
            this.addChild(this.bg_pot);
        }
    },
    initBuyChipsMenu: function () {
        if(!this.buyButton){
            var size = cc.director.getWinSize();
            var buyMenuItem = new cc.MenuItemImage(this.baseDir+'buy.png',this.baseDir+'buy.png',this.onBuyPot);
            buyMenuItem.x = size.width - 50;
            buyMenuItem.y = size.height - 100;
            this.buyButton = new cc.Menu(buyMenuItem);
            this.buyButton.setPosition(cc.p(0,0));
            this.addChild(this.buyButton);
        }
    },
    initRoomMenu: function () {
        if(!this.menuButton){
            var size = cc.director.getWinSize();
            var menuItem = new cc.MenuItemImage(this.baseDir+'menu.png',this.baseDir+'menu.png',this.onMenu,this);
            menuItem.x = 60;
            menuItem.y = size.height - 100;

            this.menuButton = new cc.Menu(menuItem);
            this.menuButton.setPosition(cc.p(0,0));
            this.addChild(this.menuButton);
        }
    },
    initHelpMenu: function () {
        if(!this.helpButton) {
            var helpButtonItem = new cc.MenuItemImage(this.baseDir + 'icon-tips.png', this.baseDir + 'icon-tips.png', this.onHelp, this);
            helpButtonItem.x = 50;
            helpButtonItem.y = 50;
            helpButtonItem.setScale(1.5);
            this.helpButton = new cc.Menu(helpButtonItem);
            this.helpButton.setPosition(cc.p(0, 0));
            this.addChild(this.helpButton);
        }
    },
    initTalkMenu: function () {
        if(!this.talkButton) {
            var size = cc.director.getWinSize();
            var talkItem = new cc.MenuItemImage(this.baseDir + 'icon-talk.png', this.baseDir + 'icon-talk.png', this.onTalk, this);
            talkItem.x = size.width - 200;
            talkItem.y = 50;
            this.talkButton = new cc.Menu(talkItem);
            this.talkButton.setPosition(cc.p(0, 0));
            this.addChild(this.talkButton);
        }
    },
    initEmotionMenu: function () {
        if(!this.emotionButton){
            var size = cc.director.getWinSize();
            var emotionItem = new cc.MenuItemImage(this.baseDir + 'icon-emotion.png',this.baseDir + 'icon-emotion.png',this.onEmotion,this);
            emotionItem.x = size.width - 100;
            emotionItem.y = 50;
            this.emotionButton = new cc.Menu(emotionItem);
            this.emotionButton.setPosition(cc.p(0,0));
            this.addChild(this.emotionButton);
        }
    },
    initPlayersPosition: function () {
        var players = Room.players;

        var size = cc.director.getWinSize();

        var position = GC.position[players.length](size);

        for(var i = 0 ; i < players.length; i++){
            if(!!players[i].id){
                var player = new Player(players[i]);

                player.x = position[ players[i].position].x;
                player.y = position[ players[i].position].y;

                this.addChild(player,2);
            }
        }

        this.initFaPaiAnimaition();

    },
    initFaPaiAnimaition: function () {
        //发牌动画
        var size = cc.director.getWinSize();

        var card_back_small = new cc.Sprite('#card-back-small.png');
        card_back_small.x = size.width / 2;
        card_back_small.y = size.height + 10;
        this.card_back_small = card_back_small;
        //todo 发牌完毕 removeChild
        this.addChild(this.card_back_small);

        var players = Room.players;
        var actions = [];

        var position = GC.position[players.length](size);
        //第一张牌
        for(var i = 0 ; i < players.length; i++){
            if(!!players[i].id){
                var x = position[ players[i].position].x;
                var y = position[ players[i].position].y - 50;

                var action = new cc.MoveTo(0.5,cc.p(x,y));
                var finish = new cc.CallFunc(this.finish1.bind(this,players[i].id,position[ players[i].position]),this,true);
                var action1 = new cc.RotateBy(0.5,360,finish);
                var spawnAction = new cc.Spawn(action,action1);
                var action2 = new cc.MoveTo(0.01,cc.p(size.width/2,size.height + 10));
                //牌旋转
                actions.push(spawnAction);
                actions.push(action2);
                actions.push(finish);
            }
        }

        for(var i = 0 ; i < players.length; i++){
            if(!!players[i].id){
                var x = position[ players[i].position].x;
                var y = position[ players[i].position].y - 50;

                var action = new cc.MoveTo(0.5,cc.p(x,y));
                var finish = new cc.CallFunc(this.finish2.bind(this,players[i].id,position[ players[i].position]),this,true);
                var action1 = new cc.RotateBy(0.5,360);
                var spawnAction = new cc.Spawn(action,action1);
                var action2 = new cc.MoveTo(0.01,cc.p(size.width/2,size.height + 10));
                //牌旋转
                actions.push(spawnAction);
                actions.push(action2);
                actions.push(finish);
            }
        }

        actions.push(new cc.CallFunc(this.convert.bind(this),this,true));
        var seqAction = new cc.Sequence(actions);
        card_back_small.runAction(seqAction);
    },
    finish1: function (id,position) {
        var card_back_small = new cc.Sprite('#card-back-small.png');
        card_back_small.x = position.x;
        card_back_small.y = position.y - 50;
        if(id == 1){
            trace('card1');
            this.card1 = card_back_small;
            this.addChild(this.card1,2);
        } else {
            this.addChild(card_back_small,2);
        }
    },
    finish2: function (id,position) {
        var card_back_small = new cc.Sprite('#card-back-small.png');
        card_back_small.x = position.x - 10;
        card_back_small.y = position.y - 50;
        if(id == 1){
            trace('card2');
            this.card2 = card_back_small;
            this.addChild(this.card2,3);
        } else {
            this.addChild(card_back_small,3);
        }
        var action = new cc.RotateBy(0.5,-15);
        card_back_small.runAction(action);
    },
    convert: function () {
        //当前玩家的牌反转过来
        trace('当前玩家的牌反转过来');
        if(!!this.card1){
            trace('card1 visible false');
            this.card1.setVisible(false);
        }
        if(!!this.card2){
            trace('card2 visible false');
            this.card2.setVisible(false);
        }

        var sprite1Name = getCardNameByNumber(4);
        trace('sprite1Name: '+sprite1Name);
        var sprite1 = new cc.Sprite('#'+sprite1Name);
        sprite1.x = this.card1.x + 10;
        sprite1.y = this.card1.y;
        sprite1.setScale(0.5);
        this.addChild(sprite1,3);

        var sprite2Name = getCardNameByNumber(8);
        trace('sprite2Name: '+sprite2Name);
        var sprite2 = new cc.Sprite('#'+sprite2Name);
        sprite2.x = this.card2.x;
        sprite2.y = this.card2.y;
        sprite2.setScale(0.5);
        this.addChild(sprite2,2);
        this.scheduleOnce(this.removeChild,0.5);
    },
    removeChild: function () {
        this.removeChild(this.card1);
        this.removeChild(this.card2);
    },
    onMenu: function () {
        if(!this.menu_layer) {
            trace('onMenu');
            //todo 构造换桌 退出 站起 菜单
            var layer = new cc.Layer();

            var size = cc.director.getWinSize();

            var menu_bg = new cc.Sprite(this.baseDir + 'bg-menu.png');
            menu_bg.x = 120;
            menu_bg.y = size.height - 250;
            layer.addChild(menu_bg, 0);

            var changeTable = new cc.MenuItemImage(this.baseDir + 'change.png', this.baseDir + 'change.png', this.onChangeTable, this);
            changeTable.x = 120;
            changeTable.y = menu_bg.y + 60;

            var changeTableMenu = new cc.Menu(changeTable);
            changeTableMenu.setPosition(cc.p(0, 0));
            layer.addChild(changeTableMenu, 1);

            //sitout.png
            var sitout = new cc.MenuItemImage(this.baseDir + 'sitout.png', this.baseDir + 'sitout.png', this.onSitOut, this);
            sitout.x = 120;
            sitout.y = menu_bg.y;

            var sitoutMenu = new cc.Menu(sitout);
            sitoutMenu.setPosition(cc.p(0, 0));
            layer.addChild(sitoutMenu, 1);

            var exit = new cc.MenuItemImage(this.baseDir + 'exit.png', this.baseDir + 'exit.png', this.onExit, this);
            exit.x = 120;
            exit.y = menu_bg.y - 60;

            var exitMenu = new cc.Menu(exit);
            exitMenu.setPosition(cc.p(0, 0));
            layer.addChild(exitMenu, 1);
            this.menu_layer = layer;

            this.addChild(this.menu_layer, 1);
        } else {
            if(!this.menu_layer.visible){
                this.menu_layer.setVisible(true);
            }
        }
    },
    onChangeTable: function () {
        //this.menu_layer.setVisible(false);
        trace('onChangeTable');
    },
    onSitOut: function () {
        trace('onSitout');
    },
    onExit: function () {
        trace('onExit');
    },
    onBuyPot: function () {
        //todo 购买筹码
        trace('buyPot');
    },
    onHelp: function () {

    },
    onEmotion: function () {
        
    },
    onTalk: function () {
        
    }
});