/**
 * Created by lixiaodong on 16/4/7.
 */
var Player = cc.Layer.extend({
    id          :   0,
    nickName    :   '',
    headImage   :   '',
    position    :   0,
    giftIcon    :   '',
    allChips    :   0,//所有筹码
    outChips    :   0,//投入筹码
    leftChips   :   0,//剩余筹码
    action      :   '', //跟注 弃牌 看牌 下注
    cards       :   [],//牌



    stauts:0,

    ctor: function (args) {
        this._super();
        var baseDir = "#img/room/";


        var nickName = args.nickName;
        //var headImage = args.headImage;
        var chips = args.chips;
        var headImage = new cc.Sprite(res.f_png);
        headImage.setScale(0.5);

        var nameLabel = new cc.LabelTTF(nickName,"Arial", 36);
        nameLabel.x = headImage.width / 2;
        nameLabel.y = headImage.height + 50;
        headImage.addChild(nameLabel,1);

        if(args.position == 1){//当前玩家
            var btn_chipin = new cc.Sprite(baseDir + 'btn-chipin.png');
            btn_chipin.x = headImage.width / 2;
            btn_chipin.y = headImage.height + 150;
            btn_chipin.setScale(2);
            headImage.addChild(btn_chipin,0);
        }

        this.addChild(headImage);

        return true;
    }
});