var baseDir = "res/";

var res = {
    logo            :   baseDir + "logo.png",
    app_name        :   baseDir + "splash.png",
    HelloWorld_png  :   baseDir + "HelloWorld.png",
    login_png       :   baseDir + "login.png",
    help_0_png      :   baseDir + "guide_01.png",
    help_1_png      :   baseDir + "guide_02.png",
    help_2_png      :   baseDir + "guide_03.png",
    help_close_png  :   baseDir + "close.png",
    bg_lobby_default:   baseDir + "index/bg-lobby-default.png",
    bg_lobby_light  :   baseDir + "index/bg-lobby-light.png",

    lobby_plist     :   baseDir + "index.plist",
    startup_plist   :   baseDir + "startup.plist",
    room_list       :   baseDir + "room.plist",
    cards_list      :   baseDir + "cards.plist",
    f_png           :   baseDir + "f.png",
    m_png           :   baseDir + "m.png",


    lobbyMusic      :   baseDir + 'ogg/bgm2.ogg'
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
