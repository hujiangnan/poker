/**
 * Created by Mrli on 16/3/6.
 */
function ajaxGet(url,cb){
    var xmlhttp;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        console.log(xmlhttp.readyState , xmlhttp.status,xmlhttp.responseText);
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            cb(JSON.parse(xmlhttp.responseText));
        }
    };
    xmlhttp.open("GET",url,true);
    xmlhttp.send();
}

function getRequest(url,cb){
    var xhr = cc.loader.getXMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
            var httpStatus = xhr.statusText;
            var response = xhr.responseText;
            response = typeof(response) == 'string' ? JSON.parse(response) : response;
            cb(response);
        }
    };
    xhr.send();
}