/**
 * Created by lixiaodong on 16/4/7.
 */
//var TestLayer = cc.Layer.extend({
//    _size : 0,
//    ctor: function () {
//        this._super();
//        this._size = cc.director.getWinSize();
//
//        this.init();
//    },
//    init: function () {
//        var clipper = this.createClipping();
//
//        var stencil = this.createStencil();
//        clipper.setStencil(stencil);          //为裁剪节点添加模板
//
//        // draw cardinal spline --- 绘制曲线
//        // drawNode.drawCardinalSpline(config, tension, segments, lineWidth, color)
//        var vertices4 = [
//            cc.p(centerPos.x - 130, centerPos.y - 130),
//            cc.p(centerPos.x - 130, centerPos.y + 130),
//            cc.p(centerPos.x + 130, centerPos.y + 130),
//            cc.p(centerPos.x + 130, centerPos.y - 130),
//            cc.p(centerPos.x - 130, centerPos.y - 130)
//        ];
//        draw.drawCardinalSpline(vertices4, 0.5, 100, 2, cc.color(255, 255, 255, 255));
//
//        var stencil = null;   //可以是精灵，也可以DrawNode画的各种图形
//        //1.创建裁剪节点
//        var clipper = new cc.ClippingNode();   //创建裁剪节点ClippingNode对象  不带模板
//        //var clipper = new cc.ClippingNode(stencil);   //创建裁剪节点ClippingNode对象  带模板
//        clipper.setInverted(false);         //显示被模板裁剪下来的底板内容。默认为false 显示被剪掉部分。
//        clipper.inverted = true;             //设置底板可见，显示剩余部分
//        //2.设置模板
//        //alpha阀值：表示像素的透明度值。
//        // 只有模板（stencil）中像素的alpha值大于alpha阈值时，内容才会被绘制。
//        //alpha阈值（alphaThreshold）：取值范围[0,1]。
//        //默认为1，表示alpha测试默认关闭，即全部绘制。
//        //若不是1，表示只绘制模板中，alpha像素大于alphaThreshold的内容。
//        clipper.setAlphaThreshold(0);    //设置绘制底板的Alpha值为0
//        clipper.alphaThreshold = 0.5;
//        //3.创建底板
//        var sprite = new cc.Sprite(res.HelloWorld_png);
//        sprite.scale = 1;
//        clipper.setStencil(sprite);        //设置模板
//
//        this.addChild(clipper);
//
//        return true;
//    },
//    createClipping: function () {
//        var clipper = new cc.ClippingNode();   //创建不带模板的裁剪节点
//        clipper.anchorX = 0.5;
//        clipper.anchorY = 0.5;
//        clipper.x = this._size.width / 2;
//        clipper.y = this._size.height / 2;
//
//        clipper.setInverted(false);
//        clipper.setAlphaThreshold(1);
//        return clipper;
//    },
//    createStencil: function () {
//        var shape = new cc.DrawNode();
//        var green = cc.color(0, 255, 0, 255);
//        shape.drawRect(cc.p(-50,-50), cc.p(100, 100), green, 2, green);
//        shape.x = shape.y = 0;
//        return shape;
//    },
//    createContent: function () {
//        var content = new cc.Sprite(res.f_png);
//        content.x = 0;
//        content.y = 0;
//        return content;
//    }
//
//});
//
//var TestScene = cc.Scene.extend({
//
//    onEnter: function () {
//        this._super();
//        var layer = new TestLayer();
//        this.addChild(layer);
//    }
//});

var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();


        var size = cc.director.getWinSize();

        //加载角色资源
        var role = new cc.Sprite('#card-105.png');
        //添加到当前图层
        role.x = size.width / 2;
        role.y = size.height / 2;
        this.addChild(role);
        //确定位置
        //缩放
        role.setScale(0.5);
        //调整人物的X轴初始值
        role.setPositionX(100);
        //调用 runaction  函数  传入一个 action  对象 moveTo 执行时间 , 目标位置
        //role.runAction(new cc.MoveTo(2,cc.p(size.width-100, size.height/2)));
        //moveBy 向量坐标
        //role.runAction(new cc.MoveBy(2,cc.p(500, 0)));

        //如何执行连续的两个动作
        /*var move1 = new cc.moveTo(2,cc.p(size.width-100, size.height/2));

         var move2 = new cc.MoveBy(2,cc.p(-500, 0));
         //创建顺序执行动作的对象 sequence
         role.runAction(new cc.Sequence(move1,move2));*/

        //反转 action
        var move1 = new cc.MoveBy(2,cc.p(500,0));
//
        var move2  = move1.reverse();
//
        role.runAction(new cc.Sequence(move1,move2));
//

        //如何并列执行
        //var move = new cc.moveBy(2,cc.p(500, 0));
        //var scale = new cc.ScaleTo(2,1);//用两秒钟时间 缩放到原来的大小
        //spawn  并列执行函数
        //role.runAction(new cc.Spawn(move,scale));

        //当前的特效是否执行完成
        //role.runAction(new cc.Sequence(new cc.Spawn(move,scale), new cc.CallFunc(function(){
        //    cc.log("play complete");
        //})));
        return true;
    }
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});