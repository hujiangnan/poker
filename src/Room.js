/**
 * Created by lixiaodong on 16/4/7.
 */
var Room = {
    players : [
        {
            id  :   1,
            nickName    :   '用户20160101',
            position:1

        },
        {
            id  :   2,
            nickName    :   '用户20160201',
            position:2
        },
        {
            id  :   3,
            nickName    :   '用户20160301',
            position:3
        },
        {
            id  :   4,
            nickName    :   '用户20160401',
            position:4
        },
        {
            id  :   5,
            nickName    :   '用户20160501',
            position:5
        }
    ]
};
/*
 ,
 {
 id  :   6,
 nickName    :   '用户20160601',
 position:6
 },
 {
 id  :   7,
 nickName    :   '用户20160701',
 position:7
 },
 {
 id  :   8,
 nickName    :   '用户20160801',
 position:8
 },
 {
 id  :   9,
 nickName    :   '用户20160901',
 position:9
 },
 {
 id  :   10,
 nickName    :   '用户20161001',
 position:10
 },
 {
 id  :   11,
 nickName    :   '用户20161101',
 position:11
 }
 */