/**
 * Created by lixiaodong on 16/4/1.
 */
var GC = {};

GC.web_host = '192.168.1.12';
GC.web_port = 8080;
GC.login = '/login';

GC.gate_host = '192.168.1.12';
GC.gate_port = 1688;

//5人间 9人间
GC.position = {
    5 : function (size) {
        var middle_x = size.width / 2;
        var middle_y = size.height / 2;

        return {
            1   :   {x  :   middle_x , y :   250},
            2   :   {x  :   80  , y :   middle_y - 50},
            3   :   {x  :   80  , y :   middle_y + 300},
            4   :   {x  :   650  , y :   middle_y + 300},
            5   :   {x  :   650  , y :   middle_y - 50}
        }
    },
    11 : function (size) {
        var middle_x = size.width / 2;
        var middle_y = size.height / 2;

        return {
            1   :   {x  :   middle_x , y :   250},
            2   :   {x  :   0.5 * middle_x   , y :   middle_y - 300},
            3   :   {x  :   middle_x - 300  , y :   middle_y - 100},
            4   :   {x  :   middle_x - 300  , y :   middle_y + 150},
            5   :   {x  :   middle_x - 300  , y :   middle_y + 400},
            6   :   {x  :   0.5 * middle_x + 80  , y :   middle_y + 550},


            7   :   {x  :   1.5 * middle_x - 80  , y :   middle_y + 550},
            8   :   {x  :   middle_x + 300  , y :   middle_y + 400},
            9   :   {x  :   middle_x + 300  , y :   middle_y + 150},
            10   :   {x  :   middle_x + 300  , y :   middle_y - 100},
            11   :   {x  :   1.5 * middle_x  , y :   middle_y - 300}
        }
    }
}
