/**
 * Created by lixd on 16/3/31.
 */
var LobbyLayer = cc.Layer.extend({
    lobbyLayer  :   null,

    ctor: function () {
        this._super();

        if(!this.lobbyLayer){
            this.lobbyLayer = new LobbyUI();
            this.addChild(this.lobbyLayer);
        }

        return true;
    }
});

var LobbyScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new LobbyLayer();
        this.addChild(layer);
    }
});