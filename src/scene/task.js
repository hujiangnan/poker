/**
 * Created by lixiaodong on 16/4/5.
 */

var TaskLayer = cc.Layer.extend({
    ctor: function () {
        this._super();

        cc.spriteFrameCache.addSpriteFrames(res.lobby_plist);

        this.initTasks();

        return true;
    },
    initTasks: function () {
        var size = cc.director.getWinSize();

        var baseDir = "#img/index/dailyTasks/";

        var bg = new cc.Sprite(baseDir+'bg-task.png');
        bg.x = size.width / 2;
        bg.y = size.height / 2;

        this.addChild(bg);
    }
});


var TaskScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new TaskLayer();
        this.addChild(layer);
    }
});