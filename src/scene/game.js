/**
 * Created by lixiaodong on 16/4/5.
 */
var GameLayer = cc.Layer.extend({
    players     :   null,
    menu        :   null,
    bg          :   null,
    bg_logo     :   null,
    bg_pot      :   null,
    buyButton   :   null,
    menuButton  :   null,
    menu_bg     :   null,
    baseDir     :   null,
    menuLayer   :   null,
    gameSceneUI :   null,


    ctor: function () {
        this._super();

        this.baseDir     =   "#img/room/";

        if ('mouse' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseDown: function (event) {
                    var pos = event.getLocation();
                    var target = event.getCurrentTarget();
                    if (event.getButton() === cc.EventMouse.BUTTON_LEFT) {
                        //console.log("onMouseLeftDown " + pos.x + " " + pos.y);
                    }
                    else if (event.getButton() === cc.EventMouse.BUTTON_RIGHT) {
                        //console.log("onMouseRightDown " + pos.x + " " + pos.y);
                    }
                },
                onMouseMove: function (event) {
                    var pos = event.getLocation();
                    var target = event.getCurrentTarget();
                    //console.log("onMouseMove " + pos.x + " " + pos.y);
                    var delta = event.getDelta();
                    //console.log('delta x:'+delta.x+' Y:'+delta.y);
                },
                onMouseUp: function (event) {
                    var pos = event.getLocation();
                    var target = event.getCurrentTarget();
                    //console.log("onMouseUp " + pos.x + " " + pos.y);
                }
            }, this);
        }else{
            console.log("Mouse Not Supported");
        }

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.log("begin");
                this.touchLaction = touch.getLocation();
                return true;
            },
            onTouchMoved: function (touch, event) {
                cc.log("move");
            },
            onTouchEnded: function (touch, event) {
                cc.log("end");
                var touchEnd = touch.getLocation();
                var delatX = this.touchLaction.x - touchEnd.x;
                //delat取50为边界，确保不会因为误操作而变动；
                if (delatX >= 50 || delatX <= -50){
                    if (delatX < -50){
                        cc.log("向右滑动咯");
                    }
                    else if (delatX >50){
                        cc.log("向左滑动咯");
                    }
                }

                var delatY = this.touchLaction.y - touchEnd.y;
                if (delatY >= 50 || delatY <= -50){
                    if (delatY < -50){
                        cc.log("向上滑动咯");
                    }
                    else if (delatY >50){
                        cc.log("向下滑动咯");
                    }
                }

            }
        }, this);


        this.gameSceneUI = new GameSceneUI();
        this.addChild(this.gameSceneUI);

        Sound.playLobbyMusic();
        return true;
    }
});

var GameScene = cc.Scene.extend({
    layer:null,
    ctor: function (args) {
        this._super();
        //trace('game scene args:'+args.params);
    },
    onEnter: function (args) {
        this._super();
        var layer = new GameLayer();
        this.addChild(layer);
    }
});